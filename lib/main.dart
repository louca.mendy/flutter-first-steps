import 'package:first_app/result.dart';
import 'package:flutter/material.dart';

import './quizz.dart';
import './result.dart';

void main() => runApp(MyApp());

class _MyAppState extends State<MyApp> {
  static const _questions = [
    {
      'questionText': 'what is your favorite rapper ?',
      'answers': [
        {'text': 'Lil Wayne', 'score': 75},
        {'text': 'ALLBLACK', 'score': 50},
        {'text': 'Jay-Z', 'score': 99}
      ],
    },
    {
      'questionText': 'what is your favorite restaurant ?',
      'answers': [
        {'text': 'Little Italy', 'score': 50},
        {'text': 'Xantos', 'score': 75},
        {'text': 'churrasqueira', 'score': 85}
      ],
    },
    {
      'questionText': 'Next city to travel',
      'answers': [
        {'text': 'Rio !', 'score': 80},
        {'text': 'Dakar', 'score': 99},
        {'text': 'Covid bruh ', 'score': 0}
      ],
    }
  ];
  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuizz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;
    if (_questionIndex < _questions.length) {
      setState(() {
        _questionIndex += 1;
      });
      print(_questionIndex);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            'The new One',
          ),
        ),
        body: _questionIndex < _questions.length
            ? Quizz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuizz),
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
    // TODO: implement createState
    throw UnimplementedError();
  }
}
