import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetQuizz;

  Result(this.resultScore, [this.resetQuizz]);

  String get resultPhrase {
    if (resultScore >= 240) {
      return 'BV BG';
    } else if (resultScore >= 200) {
      return 'Pas ouf mais y a de l\'espoir';
    }
    return 'PTDRRR trop naze';
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: [
        Text(
          resultPhrase,
          style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
        ),
        FlatButton(
          onPressed: resetQuizz,
          child: Text('Restart quizz'),
          textColor: Colors.amberAccent,
        )
      ],
    ));
  }
}
